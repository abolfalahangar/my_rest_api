## Sample Rest API With Asp.NET Core 5 with authentication

# Requirements : .Net 5 , Postgresql

- please clone this Repository with

```
$   git clone git@gitlab.com:abolfalahangar/my_rest_api.git
```

## Running the app

```bash
# development
$ dotnet run

```
