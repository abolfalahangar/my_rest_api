using System.Linq;
using Microsoft.AspNetCore.Mvc;
using My_Rest_Api.Extensions;
using My_Rest_Api.Resources;

namespace My_Rest_Api.Controllers.Config
{
    public static class InvalidModelStateResponseFactory
    {
        public static IActionResult ProduceErrorResponse(ActionContext context)
        {
            var errors = context.ModelState.GetErrorMessages();
            var response = new ErrorResource(messages: errors);
            
            return new BadRequestObjectResult(response);
        }
    }
}