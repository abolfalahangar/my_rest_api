namespace My_Rest_Api.Resources
{
    public class ProductsQueryResource : QueryResource
    {
        public int? CategoryId { get; set; }
    }
}