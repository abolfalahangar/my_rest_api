using System.ComponentModel.DataAnnotations;

namespace My_Rest_Api.Resources
{
    public class SaveCategoryResource
    {
        [Required]
        [MaxLength(30)]
        public string Name { get; set; }
    }
}