using AutoMapper;
using My_Rest_Api.Domain.Models;
using My_Rest_Api.Domain.Models.Queries;
using My_Rest_Api.Extensions;
using My_Rest_Api.Resources;

namespace My_Rest_Api.Mapping
{
    public class ModelToResourceProfile : Profile
    {
        public ModelToResourceProfile()
        {
            CreateMap<Category, CategoryResource>();

            CreateMap<Product, ProductResource>()
                .ForMember(src => src.UnitOfMeasurement,
                           opt => opt.MapFrom(src => src.UnitOfMeasurement.ToDescriptionString()));

            CreateMap<QueryResult<Product>, QueryResultResource<ProductResource>>();
        }
    }
}