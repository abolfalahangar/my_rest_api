using System.Threading.Tasks;
using My_Rest_Api.Domain.Repositories;
using My_Rest_Api.Persistence.Contexts;

namespace My_Rest_Api.Persistence.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;

        public UnitOfWork(AppDbContext context)
        {
            _context = context;     
        }

        public async Task CompleteAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}