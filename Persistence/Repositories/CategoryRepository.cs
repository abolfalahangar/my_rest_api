using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using My_Rest_Api.Domain.Models;
using My_Rest_Api.Domain.Repositories;
using My_Rest_Api.Persistence.Contexts;

namespace My_Rest_Api.Persistence.Repositories
{
    public class CategoryRepository : BaseRepository, ICategoryRepository
    {
        public CategoryRepository(AppDbContext context) : base(context) { }

        public async Task<IEnumerable<Category>> ListAsync()
        {
            return await _context.Categories.ToListAsync();

        }

        public async Task AddAsync(Category category)
        {

            await _context.Categories.AddAsync(category);

        }


        public async Task<Category> FindByNameAsync(string name)
        {
            return await _context.Categories.SingleOrDefaultAsync(c => c.Name == name);
        }

        public async Task<Category> FindByIdAsync(int id)
        {
            return await _context.Categories.FindAsync(id);
        }

        public void Update(Category category)
        {
            _context.Categories.Update(category);
        }

        public void Remove(Category category)
        {
            _context.Categories.Remove(category);
        }
    }
}