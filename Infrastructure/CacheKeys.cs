namespace My_Rest_Api.Infrastructure
{
    public enum CacheKeys : byte
    {
        CategoriesList,
        ProductsList,
    }
}