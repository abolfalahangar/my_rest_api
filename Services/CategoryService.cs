using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using My_Rest_Api.Domain.Models;
using My_Rest_Api.Domain.Repositories;
using My_Rest_Api.Domain.Services;
using My_Rest_Api.Domain.Services.Communication;
using My_Rest_Api.Infrastructure;

namespace My_Rest_Api.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly IUnitOfWork _unitOfWork;
        //private readonly IMemoryCache _cache;

        public CategoryService(ICategoryRepository categoryRepository, IUnitOfWork unitOfWork  //, IMemoryCache cache
        )
        {
            _categoryRepository = categoryRepository;
            _unitOfWork = unitOfWork;
            //_cache = cache;
        }

        public async Task<IEnumerable<Category>> ListAsync()
        {
            // if you want to cache data for 1 minute

            // var categories = await _cache.GetOrCreateAsync(CacheKeys.CategoriesList, (entry) =>
            // {
            //     entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(1);
            //     return _categoryRepository.ListAsync();
            // });

            // return categories;

            return await _categoryRepository.ListAsync();
        }

        public async Task<CategoryResponse> SaveAsync(Category category)
        {

            var result = await _categoryRepository.FindByNameAsync(category.Name);
            if (result != null)
            {
                return new CategoryResponse($"{result.Name} category already exists");
            }

            try
            {
                await _categoryRepository.AddAsync(category);
                await _unitOfWork.CompleteAsync();

                return new CategoryResponse(category);
            }
            catch (Exception ex)
            {
                // Do some logging stuff
                return new CategoryResponse($"An error occurred when saving the category: {ex.Message}");
            }
        }

        public async Task<CategoryResponse> UpdateAsync(int id, Category category)
        {
            var existingCategory = await _categoryRepository.FindByIdAsync(id);

            if (existingCategory == null)
                return new CategoryResponse("Category not found.");

            existingCategory.Name = category.Name;

            try
            {
                await _unitOfWork.CompleteAsync();

                return new CategoryResponse(existingCategory);
            }
            catch (Exception ex)
            {
                // Do some logging stuff
                return new CategoryResponse($"An error occurred when updating the category: {ex.Message}");
            }
        }

        public async Task<CategoryResponse> DeleteAsync(int id)
        {
            var existingCategory = await _categoryRepository.FindByIdAsync(id);

            if (existingCategory == null)
                return new CategoryResponse("Category not found.");

            try
            {
                _categoryRepository.Remove(existingCategory);
                await _unitOfWork.CompleteAsync();

                return new CategoryResponse(existingCategory);
            }
            catch (Exception ex)
            {
                // Do some logging stuff
                return new CategoryResponse($"An error occurred when deleting the category: {ex.Message}");
            }
        }
    }
}