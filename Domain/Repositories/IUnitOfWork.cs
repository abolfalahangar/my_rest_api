using System.Threading.Tasks;

namespace My_Rest_Api.Domain.Repositories
{
    public interface IUnitOfWork
    {
         Task CompleteAsync();
    }
}