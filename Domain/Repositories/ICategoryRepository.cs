using System.Collections.Generic;
using System.Threading.Tasks;
using My_Rest_Api.Domain.Models;

namespace My_Rest_Api.Domain.Repositories
{
    public interface ICategoryRepository
    {
        Task<IEnumerable<Category>> ListAsync();
        Task AddAsync(Category category);
        Task<Category> FindByIdAsync(int id);
        Task<Category> FindByNameAsync(string name);
        void Update(Category category);
        void Remove(Category category);
    }
}