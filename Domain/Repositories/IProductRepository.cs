using System.Collections.Generic;
using System.Threading.Tasks;
using My_Rest_Api.Domain.Models;
using My_Rest_Api.Domain.Models.Queries;

namespace My_Rest_Api.Domain.Repositories
{
    public interface IProductRepository
    {
        Task<QueryResult<Product>> ListAsync(ProductsQuery query);
        Task AddAsync(Product product);
        Task<Product> FindByIdAsync(int id);
        void Update(Product product);
        void Remove(Product product);
    }
}