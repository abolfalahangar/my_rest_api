using System.Collections.Generic;
using System.Threading.Tasks;
using My_Rest_Api.Domain.Models;
using My_Rest_Api.Domain.Services.Communication;

namespace My_Rest_Api.Domain.Services
{
    public interface ICategoryService
    {
         Task<IEnumerable<Category>> ListAsync();
         Task<CategoryResponse> SaveAsync(Category category);
         Task<CategoryResponse> UpdateAsync(int id, Category category);
         Task<CategoryResponse> DeleteAsync(int id);
    }
}