using System.Threading.Tasks;
using My_Rest_Api.Domain.Models;
using My_Rest_Api.Domain.Models.Queries;
using My_Rest_Api.Domain.Services.Communication;

namespace My_Rest_Api.Domain.Services
{
    public interface IProductService
    {
        Task<QueryResult<Product>> ListAsync(ProductsQuery query);
        Task<ProductResponse> SaveAsync(Product product);
        Task<ProductResponse> UpdateAsync(int id, Product product);
        Task<ProductResponse> DeleteAsync(int id);
    }
}