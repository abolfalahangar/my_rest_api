using System.Collections.Generic;
namespace My_Rest_Api.IdentityAuth
{
    public class Response
    {
        public string Status { get; set; }

        public string Message { get; set; }

#nullable enable
        public Dictionary<string, object>? Errors { get; set; } = new Dictionary<string, object>();
    }


}