



namespace My_Rest_Api.IdentityAuth
{

    public static class UserRoles
    {
        public const string Admin = "Admin";
        public const string User = "User";
    }
}